<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            "id"=>1,
            "name"=>"Pedro",
            "email"=>"pedro@student.com",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
        DB::table("users")->insert([
            "id"=>2,
            "name"=>"Sara",
            "email"=>"sara@student.com",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
        DB::table("products")->insert([
            "id"=>1,
            "name"=>"El Señor de los anillos",
            "category"=>1,
            "descripcion"=>"Algo de un anillo",
            "rating"=>4,
            "stock"=>7,
            "price"=>10.10,
            "image"=>"../public/img/img01",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
        DB::table("products")->insert([
            "id"=>2,
            "name"=>"Ironman",
            "category"=>2,
            "descripcion"=>"Un tio con mucha pasta",
            "rating"=>4,
            "stock"=>1,
            "price"=>5.00,
            "image"=>"../public/img/img02",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
        DB::table("products")->insert([
            "id"=>3,
            "name"=>"Passengers",
            "category"=>3,
            "descripcion"=>"Una nave espacial",
            "rating"=>2,
            "stock"=>3,
            "price"=>1.00,
            "image"=>"../public/img/img03",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
        DB::table("products")->insert([
            "id"=>4,
            "name"=>"V de Vendetta",
            "category"=>2,
            "descripcion"=>"Venganza",
            "rating"=>2,
            "stock"=>3,
            "price"=>1.00,
            "image"=>"../public/img/img04",
            "created_at"=>"null",
            "update_at"=>"null"
        ]);
    }
}
